import unittest, os, random
from yac.lib.inputs import set_service_input
from yac.lib.variables import get_variable
 

class TestCase(unittest.TestCase):

    def test_set_input_withparam(self):

        params = {"test-key": {"value": "test-value"}}
        param_key = "test-key"
        param_desc = "test key"
        param_help = "test key means this"

        set_service_input(params, param_key, param_desc, "help", 'string_wizard', True, [], "")  
        
        variable = get_variable(params,param_key)
        
        # test that the value from the params is returned
        self.assertTrue(variable == "test-value")

    def test_set_input_noparam(self):

        params = {}
        param_key = "test-key"
        param_desc = "test key"

        set_service_input(params, param_key, param_desc, "A test input", 'string_wizard', True, ['test-wiz-value'], "")  
        
        variable = get_variable(params,param_key)
        
        # test that the value from the wizard is returned
        self.assertTrue(variable == "test-wiz-value")

           