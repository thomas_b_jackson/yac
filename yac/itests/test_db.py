import unittest, os, random
from yac.lib.db import DB, get_table, create_db

#class DB():
#
#    def __init__(self, 
#                 db_name,
#                 db_user,
#                 db_pwd,
#                 db_host,
#                 db_port,
#                 db_engine):


class TestCase(unittest.TestCase):

    # that we can retrieve a table from the DB of the user's choosing
    def test_db_connection(self):
        db_host = raw_input("Input name of a postgres host >> ") 
        db_port = raw_input("Input port the host listens on >> ") 
        db_name = raw_input("Input name of db on the host >> ")
        db_user = raw_input("Input name of user on the db >> ")
        db_pwd = raw_input("Input pwd of the user on the db >> ")
        table_name = raw_input("Input name a table in the db >> ")
        db_engine = 'postgres'
 
        db = DB(db_name,
                 db_user,
                 db_pwd,
                 db_host,
                db_port,
                db_engine)


       table = get_table(table_name, db)

       self.assertTrue(table.exists())

    # that we can create a DB in an rds host
    def test_db_create(self):
        db_host = raw_input("Input name of a postgres host >> ") 
        db_port = raw_input("Input port the host listens on >> ") 
        db_name = raw_input("Input name of db to be created on the host >> ")
        sysadmin_user = raw_input("Input name of sysadmin user on the db >> ")
        sysadmin_pwd = raw_input("Input pwd of the sysadmin user on the db >> ")
        db_engine = 'postgres'

        # get the db configs for this env
        db = DB('postgres',
                 sysadmin_user,
                 sysadmin_pwd,
                 db_host,
                 db_port,
                 db_engine)

        msg = ('About to setup the "%s" DB on the %s server. ' +
               'Hit <enter> to continue...')%(db_name, db_host)

        raw_input(msg)

        # setup the DB
        create_db_sql = "CREATE DATABASE %s WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;"%db_name
        create_db(db, create_db_sql )

        self.assertTrue(True)
    