import unittest, os, random
from yac.lib.service import get_service, get_service_parmeters
from yac.lib.variables import get_variable

class TestCase(unittest.TestCase):

    def test_get_service_parameters_inputs(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        my_service = {"service-params": {"param": {"value": "testing"}},
                      "service-inputs": {"value": "yac/tests/test_vectors/service/lib/inputs.py"},
                      "service-description": {
                            "summary": "Just your basic simple service. Doesn't do much."
                      }}

        #def get_service_parmeters(service_alias, params_file, 
        #                          service_name, service_descriptor,
        #                          servicefile_path, vpc_prefs={})

        service_parmeters = get_service_parmeters(service_alias,"", 
                                                  service_name, my_service,"")

        # test that dynamic params got added by inputs.py specified by service-inputs
        dynamics_check = get_variable(service_parmeters,'test-param') == 'dynamic value!'

        self.assertTrue( dynamics_check )

    def test_get_vpc_preferences_inputs(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        my_service = {"service-params": {"param": {"value": "testing"}},
                      "service-description": {
                            "summary": "Just your basic simple service. Doesn't do much."
                      }}

        vpc_prefs = {"service-inputs": {"value": "yac/tests/test_vectors/service/lib/inputs.py"}}

        service_parmeters = get_service_parmeters(service_alias, "",
                                                  service_name, my_service,"",vpc_prefs)

        # test that dynamic params got added by inputs.py specified by service-inputs and put into vpc defaults

        dynamics_check = get_variable(service_parmeters,'vpc-defaults') and get_variable(get_variable(service_parmeters,'vpc-defaults'),'test-param') == 'dynamic value!'

        self.assertTrue( dynamics_check )                  
