import unittest, os, random
from yac.lib.container.build import download_dockerfile
from yac.lib.variables import set_variable

class TestCase(unittest.TestCase):

    # test that all of the lines gets returned, with a cr between lines
    def test_download(self): 

        repo_url = "https://bitbucket.org/ridgelinesolutions/confluence/get/master.zip"

        image_name = "nordstromsets/confluence"
        
        params = {}
        set_variable(params,
             "image-version",
             "5.8",
             "the confluence version that should be rendered into file")

        # run test
        build_path = download_dockerfile(image_name, repo_url, params)

        path_expected = os.path.join(os.path.expanduser("~"), '.yac', 'dockerfiles', image_name)

        self.assertTrue(build_path == path_expected) 
