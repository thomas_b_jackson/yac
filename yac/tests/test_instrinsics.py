import unittest, os, random, json
from yac.lib.intrinsic import apply_fxn, YAC_REF_ERROR


class TestCase(unittest.TestCase):

    # test when a list contains a list
    def test_list_of_lists(self): 
        
        test_parameters = {
            "ssl-cert" : {
              "type" : "string",
              "value": "godzilla"
            },
            "s3_path": {
               "type" : "boolean",
                "value": "/sets/jira/dev"           
            }
        }

        test_dict = {"Listeners": [
            {
              "InstancePort": {
                "Ref": "WebServerPort"
              },
              "SSLCertificateId": {
                "Fn::Join": [
                  "",
                  [
                    "arn:aws:iam::",
                    {
                      "Ref": "AWS::AccountId"
                    },
                    ":server-certificate",
                    "/",
                    {
                      "yac-ref": "ssl-cert"
                    }
                  ]
                ]
              },
              "LoadBalancerPort": "443",
              "Protocol": "HTTPS",
              "InstanceProtocol": "HTTPS"
            }
          ]
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)      

        updated_dict_str = json.dumps(updated_dict)
        
        ref_check = "godzilla" in updated_dict_str

        self.assertTrue(ref_check) 
        
    # test refs and joins
    def test_intrinsics(self): 
        
        test_parameters = {
            "comment": "the following parameters are populated by yac automatically, and can be referenced using {'yac_param': 'param-name'}",
            "cpu" : {
              "type" : "integer",
              "value": 3
            },
            "protocol" : {
              "type" : "string",
              "value": "http"
            },
            "essential": {
               "type" : "boolean",
               "value": False           
            },
            "s3_path": {
               "type" : "boolean",
                "value": "/sets/jira/dev"           
            }
        }

        test_dict = {
            "containers": [
              {
                "name": "jira", 
                "image": "nordstromsets/jira:6.3.17",
                "command": [],
                "cpu": {"yac-ref": "cpu"},
                "essential": {"yac-ref": "essential"},
                "volumesFrom": {
                    "comment": "testing",
                    "value": {"yac-join" : [ "/", [ 
                             {"yac-ref": "s3_path"},
                              "backups.json" ]]}
                },
                "memory": 2148,
                "disableNetworking": False,                
                "portMappings": [
                  {
                    "hostPort": 80,
                    "containerPort": 8090,
                    "protocol": {"yac-ref": "protocol"}
                  }
                ],
              },
              {
                "name": "confluence", 
                "image": "nordstromsets/confluence:6.3.17",
                "command": [],
                "cpu": {"yac-ref": "cpu"},
                "essential": {"yac-ref": "essential"},
                "volumesFrom": {
                    "comment": "testing",
                    "value": {"yac-join" : [ "/", [ 
                             {"yac-ref": "s3_path"},
                              "backups.json" ]]}
                },
                "memory": 2148,
                "disableNetworking": False,                
                "portMappings": [
                  {
                    "hostPort": 80,
                    "containerPort": 8090,
                    "protocol": {"yac-ref": "protocol"}
                  }
                ],
              }              
            ]
        }

        test_dict2 = {"this": {"that": "other"}, 
                     "list": [1,2,3],
                     "scalar": "test"
                     }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)

        # test that the parmeters made it in and that all the lists input made it out
        length_check = len(updated_dict['containers']) == 2
        int_check = updated_dict['containers'][0]['cpu'] == 3
        string_check = updated_dict['containers'][0]['portMappings'][0]['protocol'] == "http"
        bool_check = updated_dict['containers'][0]['essential'] == False
        join_check = updated_dict['containers'][0]['volumesFrom']['value'] == "/sets/jira/dev/backups.json"

        self.assertTrue(length_check and int_check and string_check and bool_check and join_check)

    # test join where one element is empty
    def test_null_join(self): 
        
        test_parameters = {
            "suffix" : {
              "type" : "string",
              "value": ""
            },
            "s3_path": {
               "type" : "boolean",
                "value": "/sets/jira/dev"           
            }
        }

        test_dict = {
            "volumesFrom": {
                "comment": "testing",
                "value": {"yac-join" : [ "/", [ 
                         {"yac-ref": "s3_path"},
                         {"yac-ref": "suffix"},
                          "backups.json" ]]}
            }
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)

        join_check = test_dict['volumesFrom']['value'] == "/sets/jira/dev/backups.json"

        self.assertTrue(join_check)  


    # test when a dictionary references a param that doesn't exist
    def test_reference_error(self): 
        
        test_parameters = {
            "suffix" : {
              "type" : "string",
              "value": ""
            },
            "s3_path": {
               "type" : "boolean",
                "value": "/sets/jira/dev"           
            }
        }

        test_dict = {
            "volumesFrom": {
                "comment": "testing",
                "value": {"yac-ref": "suffi"}
            }
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)     

        # we should get a reference error
        ref_check = updated_dict['volumesFrom']['value'] == '%s: %s'%(YAC_REF_ERROR,"suffi")

        self.assertTrue(ref_check)        