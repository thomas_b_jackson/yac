import unittest, os, random, shutil
from yac.lib.file import get_file_contents, FileError
from yac.lib.stack import deploy_stack_files
from yac.lib.stack import is_s3_destination

class TestCase(unittest.TestCase):

    def test_deploy_files(self):

        temp_test_dir = "yac/tests/test_vectors/deploy"
        service_parmeters = {"render-me": {"value": "baby!"},
                             "servicefile-path": {"value": temp_test_dir}}
        my_service = {"deploy-for-boot": {
            "comment": "files to deploy before starting this service for the first time",
            "files": [
                {
                    "src":  "deploy_for_boot.txt",
                    "dest": "yac/tests/test_vectors/deploy/rendered/deploy_for_boot.txt"
                }
                ]
            }
        }

        try:
            deploy_stack_files(my_service, service_parmeters,"")

        except FileError as e:
            print e.msg

        # read file contents from destination file
        file_contents = get_file_contents(my_service["deploy-for-boot"]['files'][0]['dest'])

        # clean up
        shutil.rmtree("yac/tests/test_vectors/deploy/rendered")
        shutil.rmtree("yac/tests/test_vectors/deploy/tmp")

        deploy_check = file_contents == "render my params, then deploy me %s"%(service_parmeters["render-me"]["value"])
        
        self.assertTrue(deploy_check)

    def test_deploy_dirs(self):

        temp_test_dir = "yac/tests/test_vectors/deploy"
        service_parmeters = {"render-me": {"value": "baby!"},
                             "servicefile-path": {"value": temp_test_dir}}
        my_service = {"deploy-for-boot": {
            "comment": "files to deploy before starting this service for the first time",
            "directories": [
                {
                    "src":  "sample_dir",
                    "dest": "yac/tests/test_vectors/deploy/rendered/sample_dir"
                }
                ]
            }
        }

        try:
            deploy_stack_files(my_service, service_parmeters,"")

        except FileError as e:
            print e.msg

        # read file contents from destination file
        rendered_file = '%s/%s'%(my_service["deploy-for-boot"]['directories'][0]['dest'],"sample_file1.txt")
        file_contents = get_file_contents(rendered_file)

        # clean up
        shutil.rmtree("yac/tests/test_vectors/deploy/rendered")
        shutil.rmtree("yac/tests/test_vectors/deploy/tmp")

        deploy_check = file_contents == "render my params, then deploy me %s"%(service_parmeters["render-me"]["value"])
        
        self.assertTrue(deploy_check)

    # detection of an s3-related url
    def test_is_s3_destination(self):        

        self.assertTrue(is_s3_destination("s3://test")) 