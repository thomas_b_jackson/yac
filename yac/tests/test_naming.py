import unittest, os, random
from yac.lib.naming_default import get_resource_name, get_stack_name

class TestCase(unittest.TestCase):

    def test_resource_name_asg(self):

        params = {"service-alias": {"value": "jira"}, 
                  "env": {"value": "dev"}
                }

        resource_name = get_resource_name(params, "asg")

        self.assertTrue(resource_name == 'jira-asg')
            
 
    def test_stack_name(self):

        params = {"service-alias": {"value": "jira"}, 
                  "env": {"value": "dev"}, 
                  "prefix": {"value": "tts-sets"}
                }

        resource_name = get_stack_name(params)

        self.assertTrue(resource_name == 'tts-sets-jira')
   	
 