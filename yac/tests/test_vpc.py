import unittest, os, random
from yac.lib.vpc import get_vpc_prefs_from_file

class TestCase(unittest.TestCase):

    def test_vpc_prefs(self): 
        
        prefs_file = 'yac/tests/test_vectors/vpc/vpc-prefs.json'

        # run test
        vpc_prefs = get_vpc_prefs_from_file(prefs_file)

        self.assertTrue(vpc_prefs)
        
