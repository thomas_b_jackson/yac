#!/bin/bash

# these values should get rendered in
NTP_SERVERS={{param1}}

PHOBIAS={{param2}}

# format volumes as an ext4 fs
mkfs -t ext4 /dev/xvdc
mkfs -t ext4 /dev/xvdd