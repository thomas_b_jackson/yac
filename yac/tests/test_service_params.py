import unittest, os, random
from yac.lib.service import get_service, get_service_parmeters
from yac.lib.variables import get_variable
from yac.lib.registry import get_private_registry, set_private_registry, MOCK_REGISTRY_DESC

class TestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        # save currently configured registry so it can be re-set at the 
        # conclusion of testing
        cls.current_registry = get_private_registry()

        # set private registry to a mock registry
        set_private_registry(MOCK_REGISTRY_DESC)

    @classmethod
    def tearDownClass(cls): 

        # re-set users private registry 
        set_private_registry(cls.current_registry)

    # if we get a service from file, get_service returns the correct service name and alias
    def test_get_service_file(self):        

        myservice_arg = 'yac/tests/test_vectors/service/simple_service.json'

        service_dict, service_name, file_path = get_service(myservice_arg)

        service_name_check = service_name == "nordstrom/jira"
        service_check = len(service_dict['stack-template']['task-defintion']['containers'])==2
        path_check = file_path == "yac/tests/test_vectors/service"
        self.assertTrue(service_check and service_name_check and path_check) 


    def test_get_service_parameters(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        my_service = {"service-params": {},
                      "service-description": {
                            "summary": "Just your basic simple service. Doesn't do much.",
                            "details":  ["* Looks nice"],
                            "maintainer" : {
                              "name":     "Thomas Jackson",
                              "email":    "thomas.b.jackson@gmail.com"
                            }
                        }
                    }
             
        #def get_service_parmeters(service_alias, params_file, 
        #                          service_name, service_descriptor,
        #                          servicefile_path, vpc_prefs={})

        service_parmeters = get_service_parmeters(service_alias, "", 
                                                  service_name, my_service, "")

        alias_check = get_variable(service_parmeters,'service-alias') == service_alias
        service_name_check = get_variable(service_parmeters,'service-name') == service_name

        self.assertTrue(alias_check and service_name_check )         
