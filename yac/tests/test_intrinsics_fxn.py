import unittest, os, random
from yac.lib.intrinsic import apply_fxn

class TestCase(unittest.TestCase):

    # test when a dictionary references a yac-fxn
    def test_yac_fxn(self): 
        
        test_parameters = {
            "service-alias" : {
              "type" : "string",
              "value": "myservice"
            },
            "env": {
               "type" : "string",
                "value": "dev"           
            }
        }

        test_dict = {
            "myparam": {
                "comment": "testing",
                "value": {"yac-fxn": "yac/tests/test_vectors/intrinsics/fxn.py"}
            }
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)
        
        # test that the value is populated per the value returned by fxn.py
        fxn_check = updated_dict['myparam']['value'] == "myservice"

        self.assertTrue(fxn_check) 

    # test when a dictionary references a non-existant yac-fxn
    def test_no_yac_fxn(self): 
        
        test_parameters = {
            "service-alias" : {
              "type" : "string",
              "value": "myservice"
            },
            "env": {
               "type" : "string",
                "value": "dev"           
            }
        }

        test_dict = {
            "myparam": {
                "comment": "testing",
                "value": {"yac-fxn": "yac/tests/test_vectors/intrinsics/nonexistant.py"}
            }
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)
        
        # test that the value is populated per the value returned by fxn.py
        fxn_check = updated_dict['myparam']['value'] == ""

    # test when a yac-fxn returns a dict
    def test_dict_yac_fxn(self): 
        
        test_parameters = {
            "service-alias" : {
              "type" : "string",
              "value": "myservice"
            },
            "env": {
               "type" : "string",
                "value": "dev"           
            }
        }

        test_dict = {
            "myparam": {
                "comment": "testing",
                "dict": {"yac-fxn": "yac/tests/test_vectors/intrinsics/dict.py"}
            }
        }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)
        
        # test that the value is populated per the value returned by fxn.py
        fxn_check = updated_dict['myparam']['dict']['key'] == "testvalue"

        self.assertTrue(fxn_check)                                     