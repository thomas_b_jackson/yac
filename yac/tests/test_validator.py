import unittest, os, random
from yac.lib.validator import validate_dictionary

class TestCase(unittest.TestCase):

    def test_paths_onelevel_success(self): 
        
        required_fields = ["user-name"]

        test_dictionary = {
            "user-name": {"value": "Tom Jaxon"}
        }
        
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=required_fields, 
                                         required_fields=required_fields)
        
        # test that validation succeeds (no errors)
        self.assertTrue(not val_errors)

    def test_paths_onelevel_fail(self): 
        
        required_fields = ["user-names"]

        test_dictionary = {
            "user-name": {"value": "Tom Jaxon"}
        }
        
        # test that validation returns errors
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=required_fields, 
                                         required_fields=required_fields)

        self.assertTrue(val_errors)        

    def test_paths_twolevel_success(self): 
        
        required_fields = ["user-name.value"]

        supported_keys = ["user-name"]

        test_dictionary = {
            "user-name": {"value": "Tom Jaxon"}
        }
        
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=supported_keys, 
                                         required_fields=required_fields)
        
        # test that validation succeeds (no errors)
        self.assertTrue(not val_errors)

    def test_paths_twolevel_failure(self): 
        
        required_fields = ["user-name.values"]

        supported_keys = ["user-name"]

        test_dictionary = {
            "user-name": {"value": "Tom Jaxon"}
        }
        
        # test that validation returns errors
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=supported_keys, 
                                         required_fields=required_fields)
        
        self.assertTrue(val_errors) 

    def test_paths_threelevel_success(self): 
        
        required_fields = ["user-name.value.fname"]

        supported_keys = ["user-name"]

        test_dictionary = {
            "user-name": {"value": {"fname": "Tom", "lname": "Jaxon"}}
        }
        
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=supported_keys, 
                                         required_fields=required_fields)
        
        # test that validation succeeds (no errors)
        self.assertTrue(not val_errors) 

    def test_paths_threelevel_failure(self): 
        
        required_fields = ["user-name.values.fname"]

        supported_keys = ["user-name"]

        test_dictionary = {
            "user-name": {"value": {"fname": "Tom", "lname": "Jaxon"}}
        }
        
        val_errors = validate_dictionary(test_dictionary,
                                         supported_keys=supported_keys, 
                                         required_fields=required_fields)
        
        self.assertTrue(val_errors)                       