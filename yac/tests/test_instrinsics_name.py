import unittest, os, random, json
from yac.lib.intrinsic import apply_fxn, YAC_REF_ERROR

class TestCase(unittest.TestCase):

    # test refs and joins
    def test_intrinsic_name(self):       

        test_parameters = {"service-alias": {"value": "jira"}, 
                  "env": {"value": "dev"}, 
                  "suffix": {"value": "tom"}
                }

        test_dict =  {
            "Type" : "AWS::AutoScaling::AutoScalingGroup",
            "Properties" : {
              "AvailabilityZones": { "yac-ref" : "availability-zones" },
              "LoadBalancerNames": [],
              "DesiredCapacity" : "1",
              "MaxSize": "1",
              "MinSize": "1",
              "HealthCheckGracePeriod": "600",
              "HealthCheckType": "EC2",
              "LaunchConfigurationName": { "Ref" : "AppLaunchConfig" },
              "VPCZoneIdentifier": {"Ref" : "DMZSubnets"},
              "Tags" : [ 
                { "Key": "Name", "Value" : { "yac-name" : "asg" },"PropagateAtLaunch": True },
                { "Key": "Owner", "Value": { "yac-ref": "owner" },"PropagateAtLaunch": True  },
                { "Key": "CostCenter", "Value": { "yac-ref": "cost-center" },"PropagateAtLaunch": True  }
              ]
            }
          }

        # run test
        updated_dict = apply_fxn(test_dict, test_parameters)

        name_check = updated_dict['Properties']['Tags'][0]['Value'] == 'jira-tom-dev-asg'

        self.assertTrue(name_check)  
     