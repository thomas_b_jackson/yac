import unittest, os, random
from sets import Set
from yac.lib.service import register_service, get_service_by_name, clear_service, get_all_service_names
from yac.lib.file import file_in_registry
from yac.lib.variables import get_variable
from yac.lib.registry import RegError, get_private_registry, set_private_registry, MOCK_REGISTRY_DESC

class TestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        # save currently configured registry so it can be re-set at the 
        # conclusion of testing
        cls.current_registry = get_private_registry()

        # set private registry to a mock registry
        set_private_registry(MOCK_REGISTRY_DESC)

    @classmethod
    def tearDownClass(cls): 

        # re-set users private registry 
        set_private_registry(cls.current_registry)

    def test_register_service(self): 

        service_path = 'yac/tests/test_vectors/service/simple_service.json'

        service_name = "myservice:" + str(random.randint(1, 1000))

        challenge_phrase = 'test-challenge' + str(random.randint(1, 1000))

        # register this service
        updated_dict = register_service(service_name,service_path, challenge_phrase)

        # pull service back out
        returned_service, service_name_returned = get_service_by_name(service_name)

        # print returned_service['stack-template']

        length_check=False

        if returned_service:
          # test that the service returned has the params we set
          length_check = len(returned_service['stack-template']['task-defintion']['containers']) == 2

        self.assertTrue(length_check)

    def test_list_service(self): 

        service_path = 'yac/tests/test_vectors/service/simple_service.json'

        service_name1 = "myservice:" + str(random.randint(1, 1000))
        service_name2 = "myservice:" + str(random.randint(1, 1000))

        challenge_phrase = 'test-challenge' + str(random.randint(1, 1000))

        # register this service
        register_service(service_name1,service_path, challenge_phrase)
        register_service(service_name2,service_path, challenge_phrase)

        # get all service names
        all_keys = get_all_service_names()

        # verify that all the services input got returned
        keys_registered=[service_name1,service_name2]
        length_check = (len(Set(keys_registered) & Set(all_keys)) == len(keys_registered))

        self.assertTrue( length_check ) 

    # test that a service with a python source gets registered properly
    def test_register_service_file(self): 

        service_path = 'yac/tests/test_vectors/service/simple_service.json'

        service_name = "myservice:" + str(random.randint(1, 1000))

        challenge_phrase = 'test-challenge' + str(random.randint(1, 1000))

        # register this service
        updated_dict = register_service(service_name,service_path, challenge_phrase)

        # pull service back out
        returned_service, service_name_returned = get_service_by_name(service_name)

        file_check=False

        if returned_service:
            
          # test that the service returned has file with a yac url
          file_url = get_variable(returned_service,'service-inputs'"")

          file_check = file_in_registry(file_url)

        self.assertTrue(file_check)               
