import unittest, os, random
from yac.lib.template import apply_stemplate, apply_ftemplate, apply_templates_in_dir
from yac.lib.file import get_file_contents

class TestCase(unittest.TestCase):

    # test rendering templates in a string
    def test_stemplate(self):

        test_file = 'yac/tests/test_vectors/templates/sample_file.txt'

        test_variables = {
            "user-name": {"value": "Tom Jaxon"}
        }

        # read file into string
        file_contents = get_file_contents(test_file)

        # run test
        updated_file_contents = apply_stemplate(file_contents, test_variables)

        # test that user name got rendered into the file contents
        render_check = "Tom Jaxon" in updated_file_contents

        self.assertTrue(render_check)

    # test rendering templates in a file
    def test_file_template(self):

        test_file = 'yac/tests/test_vectors/templates/sample_file.txt'

        test_variables = {
            "user-name": {"value": "Tom Jaxon"}
        }

        # run test
        updated_file_contents = apply_ftemplate(test_file, test_variables)

        # test that user name got rendered into the file contents
        render_check = "Tom Jaxon" in updated_file_contents

        self.assertTrue(render_check)

    # test rendering templates in a directory
    def test_dir_template(self):

        test_dir = 'yac/tests/test_vectors/templates/sample_dir'

        test_file1 = 'tmp/sample_file1.txt'
        test_file2 = 'tmp/sample_file2.tmp'
        test_file3 = 'tmp/sub_dir/sample_file3.txt'
        test_file4 = 'tmp/sample_binary.xls'

        test_variables = {
            "user-fname": {"value": "Tom"},
            "user-lname": {"value": "Jaxon"}
        }

        # run test
        apply_templates_in_dir(test_dir, test_variables)

        self.assertTrue('Tom Jaxon' in open(test_file1).read())

        self.assertTrue('Tom Jaxon' in open(test_file2).read())

        self.assertTrue('Tom Jaxon' in open(test_file3).read())

        # test to see if binary file copies to destination properly
        self.assertTrue(os.path.exists(test_file4))
