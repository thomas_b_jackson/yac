import unittest, os, random
from yac.lib.state import get_state_s3_bucket, get_state_s3_bucket_cached

class TestCase(unittest.TestCase):

    def test_state(self):

        bucket = get_state_s3_bucket("gibberish")

        self.assertTrue(not bucket)
