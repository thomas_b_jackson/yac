import unittest, os, random
from yac.lib.boot import get_value
from yac.lib.file import register_file, clear_file_from_registry, get_file_reg_key
from yac.lib.registry import get_private_registry, set_private_registry, MOCK_REGISTRY_DESC

def string_in_list(test_list, test_string):
    in_list=False
    for item in test_list:
        if test_string in item:
            in_list = True
            break

    return in_list

class TestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        # save currently configured registry so it can be re-set at the 
        # conclusion of testing
        cls.current_registry = get_private_registry()

        # set private registry to a mock registry
        set_private_registry(MOCK_REGISTRY_DESC)

    @classmethod
    def tearDownClass(cls): 

        # re-set users private registry 
        set_private_registry(cls.current_registry)

    # test that all of the lines gets returned, with a cr between lines
    def test_boot(self): 
        
        test_parameters = {
            "boot-file" : {
              "type" : "file to boot from",
              "value": "yac/tests/test_vectors/boot/boot.sh"
            }
        }

        # run test
        boot_list = get_value(test_parameters)

        len_check = len(boot_list)==20

        self.assertTrue(len_check) 

     # test the parameter got rendered into the boot file
    def test_boot_param(self): 
        
        test_parameters = {
            "boot-file" : {
              "type" : "boot from me",
              "value": "yac/tests/test_vectors/boot/boot.sh"
            },
            "param1" : {
              "type" : "string",
              "value": "http"
            },
            "param2" : {
              "type" : "string",
              "value": "xeno"
            }            
        }

        # run test
        boot_list = get_value(test_parameters)

        # test that http got rendered in
        param_check1 = string_in_list(boot_list, "http")
        param_check2 = string_in_list(boot_list, "xeno")

        self.assertTrue(param_check1 and param_check1)

    # test that if exclusios are included and efs is not present, that
    # the EFS_ID is included in to the returned list
    def test_boot_efs(self): 
        
        test_parameters = {
            "boot-file" : {
              "type" : "boot from me",
              "value": "yac/tests/test_vectors/boot/boot.sh"
            },
            "exclusions" : {
              "type" : "string",
              "value": ["i-elb"]
            }
        }

        # run test
        boot_list = get_value(test_parameters)

        efs_check = "export EFS_ID" in boot_list[2]

        self.assertTrue(efs_check)        

    # test that if exclusios are included and efs is present, that
    # the EFS_ID is NOT included in to the returned list
    def test_boot_efs(self): 
        
        test_parameters = {
            "boot" : {
              "type" : "file to boot from",
              "value": "yac/tests/test_vectors/boot/boot.sh"
            },
            "exclusions" : {
              "type" : "string",
              "value": ["efs"]
            }
        }

        # run test
        boot_list = get_value(test_parameters)

        efs_check = True

        for item in boot_list:
            efs_check = efs_check and "export EFS_ID" not in item

        self.assertTrue(efs_check)        

    # with boot file located in registry, test that all of the lines gets returned, 
    # with a cr between lines
    def test_reg_boot(self): 
        
        boot_file_path = "yac/tests/test_vectors/boot/boot.sh"

        # create a random value to put into registry
        file_key = "test-key-" + str(random.randint(1, 1000))
        challenge_phrase = 'test-challenge' + str(random.randint(1, 1000))

        # register file
        register_file(file_key, boot_file_path, challenge_phrase)

        # get file url
        file_url = get_file_reg_key(file_key)

        test_parameters = {
            "boot-file" : {
              "type" : "file to boot from",
              "value": file_url
            }
        }

        # run test
        boot_list = get_value(test_parameters)

        # clean up - remove the test file from the registry
        clear_file_from_registry(file_key,challenge_phrase)

        len_check = len(boot_list)==20

        self.assertTrue(len_check)               