import unittest, os, random
from yac.lib.task import get_task, get_task_names, get_task_script, get_task_inputs
from yac.lib.variables import get_variable

class TestCase(unittest.TestCase):

    def test_get_task(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        service_params = {"tasks": {
                            "comment": "Tasks that can be run via yac tasks <servicefile>",
                            "value": {
                              "backup": {
                                "comment": "take a snapshot backup of the postgres data",
                                "value": {
                                  "script": "lib/backup.py"
                                }
                              }, 
                              "restore": {
                                "comment": "restore DB from the most recent backup or from a specific point-in-time",
                                "value": {
                                  "script": "lib/restore.py"
                                },
                                "inputs": {
                                  "recovery-date-time": {
                                    "description": "Recovery Date/Time",
                                    "help":    "The date and time that the DB should be restored to",             
                                    "wizard_fxn":  {
                                      "name": "date_time_wizard"
                                    },
                                    "constraints": {
                                      "required": False
                                    }
                                  }       
                                }
                              }
                            }    
                          }
                        }             

        task = get_task("backup", service_params)

        self.assertTrue(task)

    def test_get_task_names(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        service_params = {"tasks": {
                            "comment": "Tasks that can be run via yac tasks <servicefile>",
                            "value": {
                              "backup": {
                                "comment": "take a snapshot backup of the postgres data",
                                "value": {
                                  "script": "lib/backup.py"
                                }
                              }, 
                              "restore": {
                                "comment": "restore DB from the most recent backup or from a specific point-in-time",
                                "value": {
                                  "script": "lib/restore.py",
                                  "inputs": {
                                    "recovery-date-time": {
                                      "description": "Recovery Date/Time",
                                      "help":    "The date and time that the DB should be restored to",             
                                      "wizard_fxn":  {
                                        "name": "date_time_wizard"
                                      },
                                      "constraints": {
                                        "required": False
                                      }
                                    }       
                                  }
                                }
                              }
                            }    
                          }
                        }             

        task_names = get_task_names(service_params)

        self.assertTrue(task_names and ('backup' in task_names) and ('restore' in task_names))  

    def test_get_task_script(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        service_params = {"tasks": {
                            "comment": "Tasks that can be run via yac tasks <servicefile>",
                            "value": {
                              "backup": {
                                "comment": "take a snapshot backup of the postgres data",
                                "value": {
                                  "script": "lib/backup.py"
                                }
                              }, 
                              "restore": {
                                "comment": "restore DB from the most recent backup or from a specific point-in-time",
                                "value": {
                                  "script": "lib/restore.py",
                                  "inputs": {
                                    "recovery-date-time": {
                                      "description": "Recovery Date/Time",
                                      "help":    "The date and time that the DB should be restored to",             
                                      "wizard_fxn":  {
                                        "name": "date_time_wizard"
                                      },
                                      "constraints": {
                                        "required": False
                                      }
                                    }       
                                  }
                                }
                              }
                            }    
                          }
                        }             

        task_script = get_task_script("restore", service_params)

        self.assertTrue(task_script and task_script=='lib/restore.py')                        

    def test_get_task_inputs(self):

        service_alias = 'jira'
        service_name = "bitchin/jira"
        service_params = {"tasks": {
                            "comment": "Tasks that can be run via yac tasks <servicefile>",
                            "value": {
                              "backup": {
                                "comment": "take a snapshot backup of the postgres data",
                                "value": {
                                  "script": "lib/backup.py"
                                }
                              }, 
                              "restore": {
                                "comment": "restore DB from the most recent backup or from a specific point-in-time",
                                "value": {
                                  "script": "lib/restore.py",
                                  "inputs": {
                                    "recovery": {
                                      "description": "Recovery Date/Time",
                                      "help":    "The date and time that the DB should be restored to",             
                                      "wizard_fxn":  {
                                        "name": "date_time_wizard"
                                      },
                                      "constraints": {
                                        "required": False
                                      }
                                    }       
                                  }
                                }
                              }
                            }    
                          }
                        }             

        task_inputs = get_task_inputs("restore", service_params)

        self.assertTrue(task_inputs and task_inputs['recovery'])                        