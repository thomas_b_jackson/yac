#!/usr/bin/env python
import argparse, subprocess

# roll version number in setup.py, via:
#   ...
#   version='<newvers>',
#   ...

parser = argparse.ArgumentParser(description='Build and deploy yac to PyPi.')     

# required args        
parser.add_argument('ver', help='the version to build (must match version field in setup.py' )

# pull out args
args = parser.parse_args()

# build a distro:
build_cmd = "sudo python setup.py sdist"

try:
    print("If prompted, please input your lan pwd")
    subprocess.check_output( build_cmd, stderr=subprocess.STDOUT, shell=True )
    print("build ran to completion")

except subprocess.CalledProcessError as error:
    print("build failed with error: %s"%(error.output) )


# build should have generated this file
build_output = "dist/yac-%s.tar.gz"%args.ver

# upload to pypi
# (if you don't have twine, you can install using pip install twine)
upload_cmd = "twine upload " + build_output

print("Please input your PyPi username:")

try:

    subprocess.check_output( upload_cmd, stderr=subprocess.STDOUT, shell=True )
    print("distro uploaded to PyPi")

except subprocess.CalledProcessError as error:
    print("distro upload failed with error: %s"%(error.output) )